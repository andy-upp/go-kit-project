package main

import (
	"context"
	"fmt"
	"net/url"
	"os"

	httpstransport "github.com/go-kit/kit/transport/http"
)

func main(){
	tgt,_ := url.Parse("http:://localhost::8081")
	client := httpstransport.NewClient("GET",tgt,GetUserInfo,GetUserInfo_Response)
	getUserInfo := client.Endpoint()

	ctx := context.Background()
	res,err := getUserInfo(ctx,UserRequst{Uid:102})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	userinfo := res.(UserResponse)
	fmt.Println(userinfo.Result)

}

