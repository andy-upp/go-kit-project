package Services
//定义请求对象和返回对象的端点结构，并调用service层的业务逻辑，返回一个返回对象端点
import (
	"context"
	"fmt"
	"github.com/go-kit/kit/endpoint"
)

type UserRequest struct {
	Uid int `json:"uid"`
	Method string
}

type UserResponse struct {
	Result string `json:"result"`
}

func GenUserEndpoint(userService IUserService)endpoint.Endpoint  {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		r := request.(UserRequest)//进行断言
		result := "nothing"
		if r.Method == "GET" {
			result = userService.GetName(r.Uid)
		}else if r.Method=="DELETE" {
			err := userService.DelUser(r.Uid)
			if err != nil {
				result = err.Error()
			}else {
				result = fmt.Sprint("userid为%d的用户删除成功",r.Uid)
			}
		}
		return UserResponse{Result:result},nil
	}
}