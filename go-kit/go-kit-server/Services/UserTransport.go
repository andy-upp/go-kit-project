package Services

//当收到外部请求时，对外部请求进行解码，外部请求的格式可能是rpc，http等，参数可能是json或者url等。
//对请求进行解码，然后封装到endpoint中的请求对象中
import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	mymux "github.com/gorilla/mux"
)

func DocodeUserRequest(c context.Context, r *http.Request) (interface{}, error) {
	// if r.URL.Query().Get("uid") != "" {
	// 	uid,_:= strconv.Atoi(r.URL.Query().Get("uid"))
	// 	return UserRequest{
	// 		Uid:uid,
	// 	},nil
	// }
	vars := mymux.Vars(r)
	if uid, ok := vars["uid"]; ok {
		uid, _ := strconv.Atoi(uid)
		return UserRequest{
			Uid:    uid,
			Method: r.Method,
		}, nil
	}
	return nil, errors.New("参数错误")
}

//将我们发出的响应进行编码，比较通用简单的是json
func EncodeUserResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-type", "application/json")
	return json.NewEncoder(w).Encode(response)
}
