# 使用go-kit进行单个模块开发
以预警通知模块为例子，简单示范在实际项目中如何用go-kit做开发   

#文件夹说明   
cmd：模块启动与配置文件   
warningnotices: 预警模块开发     
    分层架构说明：三层结构：Transport层，Endpoint层，Service层。Transport层主要负责与传输协议HTTP，GRPC， THRIFT等相关的逻辑，Endpoint层主要负责request／response格式的转换，以及公用拦截器相关的逻辑；Service层则专注于业务逻辑    
    respository 数据仓库主要负责与数据库 消息队列 缓存 进行通信   
    warningnotice_test testify 测试代码  
 
