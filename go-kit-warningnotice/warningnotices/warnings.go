package warningnotices

//用户预警请求
type WarningNotice struct {
	UserId           *string `json:"userId"`
	CaptureTimestamp *string `json:"captureTimestamp"`
}

//用户预警信息返回
type WarningTB struct {
	Id               int    `json:"id,omitempty"`
	UserId           string `json:"userId,omitempty"`
	WxId             string `json:"wxId,omitempty"`
	Nickname         string `json:"nickname,omitempty"`
	Avatar           string `json:"avatar,omitempty"`
	MessageInfo      string `json:"messageInfo,omitempty"`
	MessageTime      string `json:"messageTime,omitempty"`
	CaptureTimestamp string `json:"captureTimestamp,omitempty"`
}
