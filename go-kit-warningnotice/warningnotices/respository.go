package warningnotices

import (
	"context"
	"database/sql"
	kitlog "github.com/go-kit/kit/log"
)

type Repository interface {
	GetNotice(ctx context.Context, userId string, captureTimestamp string)([]WarningTB, error)
}

type repository struct {
	sqlDB *sql.DB
	log kitlog.Logger
 }

func NewRepository(s *sql.DB,l kitlog.Logger)  *repository {
	return &repository{sqlDB: s,log:l}
}


func (r repository)GetNotice(ctx context.Context,userId string, captureTimestamp string)([]WarningTB,error){
   rows, err := r.sqlDB.Query("select id,user_id, wx_id,nickname,avatar,message_info,message_time  from warning where user_id = ? and capture_timestamp = ?", userId,captureTimestamp)
   if err != nil {
	   r.log.Log("err",err)
	   return nil,err
   }
   var  wt WarningTB
   var warningTBList = []WarningTB{}
   
   id := 0
   userId = ""
   wx_id := ""
   nickname := ""
   avatar := ""
   message_info := ""
   message_time := ""
   for rows.Next(){
	   rows.Scan(&id,&userId,&wx_id,&nickname,&avatar,&message_info,&message_time)
	   wt.Id = id
	   wt.UserId = userId
	   wt.WxId = wx_id
	   wt.Nickname = nickname
	   wt.Avatar = avatar
	   wt.MessageInfo = message_info
	   wt.MessageTime = message_time
	   warningTBList = append(warningTBList,wt)
   }    
	return warningTBList,nil
}


