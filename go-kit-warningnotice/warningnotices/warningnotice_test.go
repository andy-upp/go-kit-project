package warningnotices

import (
	"context"
	"database/sql"
	"testing"
	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/suite"
)

type WarningNoticeSuite struct {
	suite.Suite
	WarningNoticeSvc *warningNoticeService
}


func TestMysql(t *testing.T) {
	logger := NewKitLogger()
	db,err := sql.Open("mysql","root:123456@tcp(127.0.0.1:3306)/warning")
	if err != nil {
		logger.Log("err",err)
	}
	defer db.Close()
	wr := NewRepository(db,logger)
	ws := NewWarningNoticeService(wr,logger)
	warningtNoticeSuite := &WarningNoticeSuite{
		  WarningNoticeSvc: ws,
	}
	suite.Run(t,warningtNoticeSuite)
}

func (suite *WarningNoticeSuite)TestGetNoticeByTime() {
		var ws WarningNotice
		userInfo := "user1"
		dateInfo := "2020-06-05"
		ws.UserId = &userInfo
		ws.CaptureTimestamp = &dateInfo
		logger := NewKitLogger()
		var ctx context.Context
		wn,err := suite.WarningNoticeSvc.GetNoticeByTime(ctx,ws)
		if err != nil {
			logger.Log("err",err)
		}
		suite.Require().NotNil(wn)
}