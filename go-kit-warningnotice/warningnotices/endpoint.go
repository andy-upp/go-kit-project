package warningnotices

import (
	"context"
	"os"
	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
)

//请求预警通知端点
type warningNoticeRequest struct {
	Body WarningNotice `json:"body"`
}
type warningNoticeResponse struct {
	Result []WarningTB `json:"result,omitempty"`
	Error  string      `json:"error,omitempty"`
}

func NewKitLogger() kitlog.Logger {
	var logger kitlog.Logger
	logger = kitlog.NewJSONLogger(os.Stdout)
	logger = kitlog.With(logger, "time", kitlog.DefaultTimestamp)
	logger = kitlog.With(logger, "caller", kitlog.DefaultCaller)
	return logger
}

func makeWarningNoticeEndpoint(warningNoticeService WarningNoticeService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		logger := NewKitLogger()
		r := request.(warningNoticeRequest)
		result, err := warningNoticeService.GetNoticeByTime(ctx, r.Body)
		if err != nil {
			logger.Log("err", err)
			return warningNoticeResponse{Error: err.Error()}, nil
		}
		return warningNoticeResponse{Result: result}, nil
	}
}
