package warningnotices

import (
	"context"
	kitlog "github.com/go-kit/kit/log"
)

type WarningNoticeService interface {
	GetNoticeByTime(ctx context.Context, body WarningNotice) ([]WarningTB, error)
}

type warningNoticeService struct {
	r Repository
	l kitlog.Logger
}

func NewWarningNoticeService(r Repository, l kitlog.Logger) *warningNoticeService {
	return &warningNoticeService{
		r: r,
		l: l,
	}
}

func (w warningNoticeService) GetNoticeByTime(ctx context.Context, body WarningNotice) ([]WarningTB, error) {
	
	result, err := w.r.GetNotice(ctx,*body.UserId, *body.CaptureTimestamp)
    if err != nil {
		w.l.Log("err",err)
		return nil,err
	}
	return result, nil
}
