DROP DATABASE IF EXISTS dash;
CREATE DATABASE dash character set UTF8mb4 collate utf8mb4_bin;
USE dash;

DROP TABLE IF EXISTS member_user;
DROP TABLE IF EXISTS chatroom_member;
DROP TABLE IF EXISTS collector_member;
DROP TABLE IF EXISTS collector_user;
DROP TABLE IF EXISTS member;
CREATE TABLE member
(
    id               VARCHAR(128),
    nickname         VARCHAR(32),
    usedNickname     JSON,
    age              INT,
    sex              VARCHAR(32),
    avatar           TEXT,
    watched          boolean,
    tags             JSON,
    signature        VARCHAR(128),
    dataSource       VARCHAR(32),
    name             VARCHAR(32),
    usedName         JSON,
    email            varchar(64),
    phoneNumber      JSON,
    idCard           VARCHAR(32),
    idCardAvatar     TEXT,
    address          JSON,
    residenceAddress VARCHAR(255),
    censusAddress    VARCHAR(128),
    usedSignature    VARCHAR(128),
    captureTimestamp BIGINT,
    createTimestamp  BIGINT,
    updateTimestamp  BIGINT,
    comment          TEXT,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS chatroom;
CREATE TABLE chatroom
(
    id               VARCHAR(128),
    name             VARCHAR(128),
    dataSource       VARCHAR(32),
    avatar           TEXT,
    ownerId          varchar(128),
    usedName         JSON,
    watched          BOOLEAN,
    captureTimestamp BIGINT,
    categoryId       VARCHAR(255),
    tags             JSON,
    comment          TEXT,
#     FOREIGN KEY (ownerId) REFERENCES member(id),
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS collector;
CREATE TABLE collector
(
    id   VARCHAR(128),
    name VARCHAR(128),
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS organization;
CREATE TABLE organization
(
    id       VARCHAR(128),
    name     VARCHAR(128),
    parentId VARCHAR(128),
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS role;
CREATE TABLE role
(
    id   VARCHAR(255),
    role VARCHAR(255),
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS user;
CREATE TABLE user
(
    id             VARCHAR(255),
    username       VARCHAR(128),
    password       VARCHAR(128),
    weChatId       VARCHAR(128),
    avatar         VARCHAR(255),
    workingAddress TEXT,
    phoneNumber    VARCHAR(128),
    name           VARCHAR(128),
    levels         JSON,
    organId        VARCHAR(128),
#     FOREIGN KEY (organId) REFERENCES organization (id),
    PRIMARY KEY (id)
);

CREATE TABLE chatroom_member
(
    chatroomId VARCHAR(128),
    memberId   VARCHAR(128),
    alias      VARCHAR(128),
    quit       BOOLEAN,
    joinMethod VARCHAR(128),
    inviterId  VARCHAR(128),
#     FOREIGN KEY (chatroomId) REFERENCES chatroom(id),
#     FOREIGN KEY (memberId) REFERENCES member(id),
    UNIQUE (chatroomId, memberId)
);

CREATE TABLE collector_member
(
    collectorId VARCHAR(128),
    memberId    VARCHAR(128),
#     FOREIGN KEY (collectorId) REFERENCES collector (id),
#     FOREIGN KEY (memberId) REFERENCES member (id),
    UNIQUE (collectorId, memberId)
);

CREATE TABLE collector_chatroom
(
    collectorId VARCHAR(128),
    chatroomId  VARCHAR(128),
#     FOREIGN KEY (collectorId) REFERENCES collector (id),
#     FOREIGN KEY (chatroomId) REFERENCES chatroom (id),
    UNIQUE (collectorId, chatroomId)
);

CREATE TABLE collector_user
(
    collectorId VARCHAR(128),
    userId      VARCHAR(64),
#     FOREIGN KEY (collectorId) REFERENCES collector (id),
#     FOREIGN KEY (userId) REFERENCES user (id),
    UNIQUE (collectorId, userId)
);

DROP TABLE IF EXISTS topic;
CREATE TABLE topic
(
    id              VARCHAR(255),
    name            VARCHAR(255) NOT NULL,
    enabled         BOOLEAN      NOT NULL,
    isDefault       BOOLEAN      NOT NULl,
    keywords        JSON,
    beginTimestamp  BIGINT,
    endTimestamp    BIGINT,
    createTimestamp BIGINT,
    updateTimestamp BIGINT,
    parentId        VARCHAR(255),
    comment         TEXT,
    version         BIGINT,
#     FOREIGN KEY (parentId) REFERENCES topic (id) ON DELETE CASCADE,
    PRIMARY KEY (id),
    INDEX (name)
);

DROP TABLE IF EXISTS category;
CREATE TABLE category
(
    id              VARCHAR(255) PRIMARY KEY,
    name            VARCHAR(255) NOT NULL,
    parentId        VARCHAR(255),
    createTimestamp BIGINT       NOT NULL
);

DROP TABLE IF EXISTS file;
CREATE TABLE file
(
    id                 VARCHAR(128) NOT NULL,
    name               VARCHAR(128) NOT NULL,
    firstSeenTimestamp BIGINT,
    lastSeenTimestamp  BIGINT,
    size               BIGINT,
    md5                CHAR(32),
    type               VARCHAR(16),
    format             VARCHAR(16),
    tags               VARCHAR(128),
    count              INT,
    path               VARCHAR(128),
    PRIMARY KEY (id),
    INDEX (name),
    INDEX (firstSeenTimestamp),
    INDEX (lastSeenTimestamp)
);

DROP TABLE IF EXISTS chatroom_watched;
CREATE TABLE chatroom_watched
(
    chatroomId VARCHAR(128),
    userId VARCHAR(128),
    addTimestamp BIGINT,
    UNIQUE (chatroomId, userId),
    INDEX (userId),
    INDEX (addtimestamp)
);

DROP TABLE IF EXISTS member_watched;
CREATE TABLE member_watched
(
    memberId VARCHAR(128),
    userId VARCHAR(128),
    addTimestamp BIGINT,
    UNIQUE (memberId, userId),
    INDEX (userId),
    INDEX (addtimestamp)
);

DROP TABLE IF EXISTS notification;
CREATE TABLE notification
(
    userId    VARCHAR(128),
    messageId VARCHAR(255),
    PRIMARY KEY (userId)
);


DROP TABLE IF EXISTS message_watched;
CREATE TABLE message_watched
(
    userId       VARCHAR(128),
    messageId    VARCHAR(255),
    addTimestamp BIGINT,
    UNIQUE KEY (userId, messageId),
    INDEX (addTimestamp)
);