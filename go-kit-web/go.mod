module dash

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.2.0
	github.com/Shopify/sarama v1.26.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-kit/kit v0.10.0
	github.com/go-playground/validator/v10 v10.3.0
	github.com/go-redis/redis/v8 v8.0.0-beta.5
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.0.0
	github.com/gorilla/mux v1.7.4
	github.com/klauspost/compress v1.10.10 // indirect
	github.com/olivere/elastic/v7 v7.0.17
	github.com/pierrec/lz4 v2.5.2+incompatible // indirect
	github.com/rcrowley/go-metrics v0.0.0-20200313005456-10cdbea86bc0 // indirect
	github.com/stretchr/testify v1.5.1
	go.mongodb.org/mongo-driver v1.3.4
	golang.org/x/net v0.0.0-20200625001655-4c5254603344 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
