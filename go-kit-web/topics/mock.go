package topics

import (
	"context"
	"github.com/stretchr/testify/mock"
)

type mockRepository struct {
	mock.Mock
}

func (r *mockRepository) Delete(ctx context.Context, id string) error {
	args := r.Called(ctx, id)
	return args.Error(0)
}

func (r *mockRepository) Get(ctx context.Context, id string) (*Topic, error) {
	args := r.Called(ctx, id)
	return args.Get(0).(*Topic), args.Error(1)
}

func (r *mockRepository) Search(ctx context.Context, opts SearchOpts, args ...interface{}) ([]*Topic, error) {
	args2 := r.Called(append([]interface{}{ctx, opts}, args...)...)
	return args2.Get(0).([]*Topic), args2.Error(1)
}

func (r *mockRepository) Save(ctx context.Context, topic *Topic) error {
	args := r.Called(ctx, topic)
	return args.Error(0)
}
