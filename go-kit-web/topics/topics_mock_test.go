package topics

// import (
// 	"os"
// 	"testing"

// 	"github.com/go-kit/kit/log"
// 	"github.com/stretchr/testify/mock"
// 	"github.com/stretchr/testify/suite"
// )

// type TopicMockTestSuite struct {
// 	s Service
// 	suite.Suite
// }

// func TestTopicMockTestSuite(t *testing.T) {
// 	logger := log.NewJSONLogger(os.Stdout)
// 	repo := &mockRepository{}
// 	repo.On("Save", mock.Anything, mock.Anything).Return(nil)
// 	svc := NewService(repo, &inmemNumberService{n: 1}, logger)
// 	testingSuite := &TopicMockTestSuite{
// 		s: svc,
// 	}
// 	suite.Run(t, testingSuite)
// }

// func (suite *TopicMockTestSuite) TestTopic() {
// 	keywords := []Keywords{
// 		{Op: "include", Keywords: "非法集资 \\n可恨"},
// 		{Op: "exclude", Keywords: "123"},
// 		{Op: "include"},
// 	}
// 	topic := &Topic{
// 		Name:      "topic1",
// 		Enabled:   true,
// 		IsDefault: false,
// 		Keywords:  keywords,
// 	}
// 	ret, err := suite.s.CreateTopic(nil, topic)
// 	suite.Require().Nil(err)
// 	suite.Require().NotEqual(len(ret.Id), 0)
// 	suite.Require().Equal(topic.Name, ret.Name)
// 	suite.Require().Equal(2, len(ret.Keywords))
// 	suite.Require().Greater(ret.Version, int64(0))
// 	suite.Require().Greater(ret.CreateTimestamp, int64(0))
// 	suite.Require().GreaterOrEqual(ret.UpdateTimestamp, ret.CreateTimestamp)

// 	var str1 = "this is test"
// 	ret.Comment = &str1
// 	_, err = suite.s.UpdateTopic(nil, ret)
// 	suite.Require().Nil(err)
// }
