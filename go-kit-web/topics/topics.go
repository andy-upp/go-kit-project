/*
	topics是专题管理的相关模块,主要有如下几个部分的功能与服务:关键字专题的增删改查;智能专题的获取;关键字专题在redis中的异步刷新服务.
*/
package topics

type Keywords struct {
	Op       string `json:"op,omitempty"`
	Keywords string `json:"keywords,omitempty"`
}

/**
create topic request
{
  "name": "xxx",
  "enabled": true,
  "isDefault": false,
  "keywords": [
    {
      "op": "include",
      "keywords": "k1 k2 k3"
    },
    {
      "op": "exclude",
      "keywords": "k4 k5 k6"
    }
  ]
}
*/

type SmartTopic struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Children []SmartTopic `json:"children,omitempty"`
}

type SimpleKeywordTopic struct {
	Id       string               `json:"id"`
	Name     string               `json:"name"`
	Children []*SimpleKeywordTopic `json:"children,omitempty"`
	Parent   *SimpleKeywordTopic  `json:"-"`
}

type Topic struct {
	Id              string     `json:"id"`
	Name            string     `json:"name"`
	Enabled         bool       `json:"enabled"`
	IsDefault       bool       `json:"default,omitempty"`
	Keywords        []Keywords `json:"keywords,omitempty"`
	BeginTimestamp  *int64     `json:"beginTimestamp,omitempty"`
	EndTimestamp    *int64     `json:"endTimestamp,omitempty"`
	CreateTimestamp int64      `json:"createTimestamp,omitempty"`
	UpdateTimestamp int64      `json:"updateTimestamp,omitempty"`
	ParentId        *string    `json:"parentId,omitempty"`
	Children        []Topic    `json:"children,omitempty"`
	Comment         *string    `json:"comment,omitempty"`
	Version         int64      `json:"version,omitempty"`
}

func (t *Topic) SetKeywords(keywords []Keywords) *Topic {
	t.Keywords = keywords
	return t
}

func (t *Topic) SetBeginTimestamp(ts int64) *Topic {
	t.BeginTimestamp = &ts
	return t
}

func (t *Topic) SetEndTimestamp(ts int64) *Topic {
	t.EndTimestamp = &ts
	return t
}

func (t *Topic) SetParentId(parentId string) *Topic {
	t.ParentId = &parentId
	return t
}

func (t *Topic) SetComment(comment string) *Topic {
	t.Comment = &comment
	return t
}
