package topics

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"os"
	"testing"

	"github.com/go-kit/kit/log"
	redis "github.com/go-redis/redis/v8"
	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/suite"
)

type TopicTestSuite struct {
	s Service
	suite.Suite
	ctx context.Context
	svc KeywordTopicTreeService
}

func TestTopicTestSuite(t *testing.T) {
	logger := log.NewJSONLogger(os.Stdout)
	logger = log.With(logger, "ts", log.DefaultTimestamp)
	logger = log.With(logger, "caller", log.DefaultCaller)

	ctx := context.Background()
	var repo Repository
	db, err := sql.Open("mysql", "root:nil@tcp(192.168.31.36:13306)/dash")
	if err != nil {
		_ = logger.Log("mysql connection error", err.Error())
		os.Exit(0)
	}
	rClient := redis.NewClient(&redis.Options{
		Network: "tcp",
		Addr:    "127.0.0.1:6379",
		// Addr: "192.168.31.207:6379",
		DB: 0,
	})
	repo = NewRepository(db, logger)
	s := NewService(repo, NewNumberService("topic_version", rClient), logger)

	svc := NewKeywordTopicTreeService(repo)
	testingSuite := &TopicTestSuite{s: s, ctx: ctx, svc: svc}
	suite.Run(t, testingSuite)
}

func (suite *TopicTestSuite) TestCreateTopic() {
	//keywords := []Keywords{
	//	{Op: "include", Keywords: "非法集资 \n可恨"},
	//	{Op: "exclude", Keywords: "123"},
	//	{},
	//}
	//topic := &Topic{
	//	Name:      "topic12",
	//	Enabled:   true,
	//	IsDefault: false,
	//	Keywords:  keywords,
	//}
	//topic, err := suite.s.CreateTopic(context.Background(), topic)
	//suite.Require().Nil(err)
	//suite.Require().Equal(2, len(topic.Keywords))
	result, err := suite.svc.Get(suite.ctx)
	suite.Require().Nil(err)
	data, err := json.Marshal(result)
	suite.Require().Nil(err)
	fmt.Println(string(data))
}

//func (trts *TopicTestSuite) TestUpdateTopic() {
//	var str1 = string(`[{"op":"required", "keywords":"非法 \n可恨"},{"op":"nonrequired", "keywords":"李嘉诚 \n  \t可恨"}]`)
//	var num int64 = 1231231232
//	err := trts.s.UpdateTopic(trts.ctx, "涉金融", "涉金融", true, true, &str1, nil, nil, &num, nil, nil)
//	trts.Require().Nil(err)
//}

// func (trts *TopicTestSuite) TestGetTopic() {
// 	var id = "涉警"
// 	topic, err := trts.s.GetTopic(trts.ctx, id)
// 	fmt.Println(*topic)
// 	trts.Require().Nil(err)
// }
