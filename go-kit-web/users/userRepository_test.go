package users

import (
	"context"
	"dash/categories"
	"dash/jwt"
	"database/sql"
	"github.com/stretchr/testify/suite"
	"log"
	"testing"
)

type UserSuite struct {
	suite.Suite
	UserServ *defaultUserService
}

func TestDefaultUserService_Login(t *testing.T) {
	db, err := sql.Open("mysql",
		"root:123456@tcp(127.0.0.1:3306)/dash")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	logger := categories.NewLogger()
	ur := NewUserRepository(db, logger)
	jwts := jwt.NewJWTService(jwt.SIGNKEY)
	us := NewUserService(logger, ur, jwts)
	userSuite := &UserSuite{
		UserServ: us,
	}
	suite.Run(t, userSuite)
}

func (suite *UserSuite) TestLogin() {
	token, err := suite.UserServ.Login(context.Background(), "1", "123456")
	suite.Require().Nil(err)
	suite.Require().NotNil(token)
	_ = suite.UserServ.logger.Log("token:", token)
}

func (suite *UserSuite) TestEmptyUsernameLogin() {
	token, err := suite.UserServ.Login(context.Background(), "1", "123456")
	suite.Require().Nil(err)
	suite.Require().NotNil(token)
	_ = suite.UserServ.logger.Log("token:", token)
}

func (suite *UserSuite) TestRegister() {
	roles := make([]string, 0, 2)
	roles = append(roles, "0")
	err := suite.UserServ.Register(context.Background(), "6", "123456", roles, "A")
	suite.Require().Nil(err)
}

func (suite *UserSuite) TestRegisterEmptyUser() {
	roles := make([]string, 0, 2)
	roles = append(roles, "0")
	err := suite.UserServ.Register(context.Background(), "4", "123456", roles, "B")
	suite.Require().NotNil(err)
}

func (suite *UserSuite) TestUserRepository_Get() {
	user, err := suite.UserServ.repo.GetByUsername(context.Background(), "1")
	suite.Require().Nil(err)
	_ = suite.UserServ.logger.Log("get user:", user.Username, "uid:", user.Id, "pwd:", user.Password, "roles", user.Roles)
}
