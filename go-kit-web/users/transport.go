package users

import (
	"context"
	"dash/jwt"
	"encoding/json"
	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
)

func MakeLoginHandler(us UserService) http.Handler {
	loginHandler := kithttp.NewServer(
		makeLoginEndpoint(us),
		decodeLoginRequest,
		encodeResponse,
	)
	registerHandler := kithttp.NewServer(
		makeRegisterEndpoint(us),
		decodeRegisterRequest,
		encodeResponse,
	)
	logoutHandler := kithttp.NewServer(
		makeLogoutEndpoint(us),
		decodeLogoutRequest,
		kithttp.EncodeJSONResponse,
	)
	r := mux.NewRouter()
	r.Handle("/api/v1/user/login", loginHandler).Methods("POST")
	r.Handle("/api/v1/user/logout", logoutHandler).Methods("POST")
	r.Handle("/api/v1/user/register", registerHandler).Methods("POST")
	return r
}

func MakeUserHandler(us UserService, _ log.Logger) http.Handler {
	var profileHandler http.Handler
	{
		profileHandler = kithttp.NewServer(
			makeProfileEndpoint(us),
			decodeProfileRequest,
			encodeResponse,
		)
	}
	r := mux.NewRouter()
	r.Handle("/api/v1/user/profile", profileHandler).Methods("GET")
	return r
}

func decodeLoginRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, err
	}
	return LoginRequest{
		Username: body.Username,
		Password: body.Password,
	}, nil
}

func decodeLogoutRequest(_ context.Context, r *http.Request) (interface{}, error) {
	u := r.Context().Value("user")
	if u == nil {
		return logoutRequest{}, nil
	}
	user := u.(*jwt.User)
	return logoutRequest{Username: user.Username}, nil
}

func decodeRegisterRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Username string   `json:"username"`
		Password string   `json:"password"`
		Roles    []string `json:"roles"`
		Level    string   `json:"level"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, err
	}
	return RegisterRequest{
		Username: body.Username,
		Password: body.Password,
		Roles:    body.Roles,
		Level:    body.Level,
	}, nil
}

func decodeProfileRequest(_ context.Context, r *http.Request) (interface{}, error) {
	user := r.Context().Value("user").(*jwt.User)
	return ProfileRequest{
		Id:       user.Id,
		Username: user.Username,
		Roles:    user.Roles,
	}, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
