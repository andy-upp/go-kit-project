package users

import "dash/collectors"

type User struct {
	Id             *string                 `json:"id,omitempty"`
	Username       *string                 `json:"username,omitempty"`
	Name           *string                 `json:"name,omitempty"`
	Password       *string                 `json:"password,omitempty"`
	WeChatId       *string                 `json:"weChatId,omitempty"`
	Roles          []string                `json:"roles,omitempty"`
	Avatar         *string                 `json:"avatar,omitempty"`
	WorkingAddress *string                 `json:"workingAddress,omitempty"`
	PhoneNumber    *string                 `json:"phoneNumber,omitempty"`
	Collectors     []*collectors.Collector `json:"collectors,omitempty"`
	Level          *string                 `json:"level,omitempty"`
}

func (u *User) SetId(id string) *User {
	u.Id = &id
	return u
}

func (u *User) SetName(name string) *User {
	u.Name = &name
	return u
}

func (u *User) SetAvatar(avatar string) *User {
	u.Avatar = &avatar
	return u
}

func (u *User) SetUsername(username string) *User {
	u.Username = &username
	return u
}

func (u *User) SetPassword(password string) *User {
	u.Password = &password
	return u
}

func (u *User) SetLevel(level string) *User {
	u.Level = &level
	return u
}
