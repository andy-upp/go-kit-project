package users

import (
	"context"
	"dash/collectors"
	"github.com/go-kit/kit/endpoint"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Token string `json:"token,omitempty"`
	Err   string `json:"error,omitempty"`
}

type logoutRequest struct {
	Username string
}

func makeLoginEndpoint(us UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(LoginRequest)
		token, err := us.Login(ctx, req.Username, req.Password)
		if err == nil {
			return LoginResponse{Token: token}, nil
		}
		return LoginResponse{Err: err.Error()}, nil
	}
}

func makeLogoutEndpoint(svc UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(logoutRequest)
		err := svc.Logout(ctx, req.Username)
		if err != nil {
			return logoutResponse{Error: err.Error()}, nil
		}
		return logoutResponse{}, nil
	}
}

type RegisterRequest struct {
	Username string   `json:"username"`
	Password string   `json:"password"`
	Roles    []string `json:"roles"`
	Level    string   `json:"level"`
}

type logoutResponse struct {
	Error string `json:"error,omitempty"`
}

type RegisterResponse struct {
	Err string `json:"error,omitempty"`
}

type ProfileRequest struct {
	Id       string
	Username string
	Roles    []string
}

type ProfileResponse struct {
	Id             string                  `json:"id,omitempty"`
	Username       string                  `json:"username"`
	Name           string                  `json:"name,omitempty"`
	Roles          []string                `json:"roles,omitempty"`
	WeChatId       string                  `json:"weChatId,omitempty"`
	Avatar         string                  `json:"avatar,omitempty"`
	WorkingAddress string                  `json:"workingAddress,omitempty"`
	PhoneNumber    string                  `json:"phoneNumber,omitempty"`
	Collectors     []*collectors.Collector `json:"collectors,omitempty"`
	Error          string                  `json:"error,omitempty"`
}

func makeRegisterEndpoint(us UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(RegisterRequest)
		err = us.Register(ctx, req.Username, req.Password, req.Roles, req.Level)
		if err == nil {
			return RegisterResponse{}, nil
		}
		return RegisterResponse{Err: err.Error()}, nil
	}
}

func makeProfileEndpoint(us UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(ProfileRequest)
		res, err := us.GetProfile(ctx, req.Username)
		if err == nil {
			var userProfile ProfileResponse
			if res.Id != nil {
				userProfile.Id = *res.Id
			}
			if res.Username != nil {
				userProfile.Username = *res.Username
			}
			if res.Name != nil {
				userProfile.Name = *res.Name
			}
			if res.WeChatId != nil {
				userProfile.WeChatId = *res.WeChatId
			}
			if res.Roles != nil {
				userProfile.Roles = res.Roles
			}
			if res.Avatar != nil {
				userProfile.Avatar = *res.Avatar
			}
			if res.WorkingAddress != nil {
				userProfile.WorkingAddress = *res.WorkingAddress
			}
			if res.PhoneNumber != nil {
				userProfile.Collectors = res.Collectors
			}
			return userProfile, nil
		}
		return ProfileResponse{
			Error: err.Error(),
		}, nil
	}
}
