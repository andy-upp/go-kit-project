package notifications

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

func makeGetNotificationAlarmCountEndpoint(svc NotificationService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getNotificationAlarmCountRequest)
		count, err := svc.GetCount(ctx, req.UserId)
		if err != nil {
			return getNotificationAlarmCountResponse{Error: err.Error()}, nil
		}
		return getNotificationAlarmCountResponse{Count: count}, nil
	}
}

func makeGetNotificationAlarmEndpoint(svc NotificationService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getNotificationAlarmRequest)
		if req.Size <= 0 {
			req.Size = 20
		}
		alarms, err := svc.GetAlarms(ctx, req.UserId, req.Size, req.Data)
		if err != nil {
			return getNotificationAlarmResponse{Error: err.Error()}, nil
		}
		return getNotificationAlarmResponse{Data: alarms}, nil
	}
}
