package notifications

import "dash/messages"

type Alarm struct {
	Id        string          `json:"id"`
	Content   string          `json:"content"`
	Timestamp int64           `json:"timestamp"`
	Member    messages.Member `json:"member"`
}
