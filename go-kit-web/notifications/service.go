package notifications

import (
	"context"
)

type NotificationService interface {
	GetCount(ctx context.Context, userId string) (int, error)
	GetAlarms(ctx context.Context, userId string, size int, afterAlarm Alarm) ([]Alarm, error)
}

type notificationService struct {
	repo Repository
}

func New(repo Repository) NotificationService {
	return &notificationService{
		repo: repo,
	}
}

func (svc *notificationService) GetCount(ctx context.Context, userId string) (int, error) {
	return svc.repo.GetCount(ctx, userId)
}

func (svc *notificationService) GetAlarms(ctx context.Context, userId string, size int, afterAlarm Alarm) ([]Alarm, error) {
	return svc.repo.GetAlarms(ctx, userId, size, afterAlarm)
}
