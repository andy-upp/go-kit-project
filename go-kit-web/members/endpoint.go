package members

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"strings"
)

type getCardRequest struct {
	Id     string
	UserId string
}

type updateCardRequest struct {
	Id string `json:"id"`
	MemberBasicInfo
}

type updateCardResponse struct {
	Id    string `json:"id,omitempty"`
	Error string `json:"error,omitempty"`
}

type updateDetailRequest struct {
	Id     string           `json:"id"`
	Basic  MemberBasicInfo  `json:"basic"`
	Detail MemberDetailInfo `json:"detail"`
	Other  memberOtherInfo  `json:"other"`
}

type updateDetailResponse struct {
	Id    string `json:"id,omitempty"`
	Error string `json:"error,omitempty"`
}

type getDetailRequest struct {
	Id     string
	UserId string
}

type memberOtherInfo struct {
	CollectorOrgans  []string `json:"collectorOrgans"`
	Users            []string `json:"users"`
	Collectors       []string `json:"collectors"`
	CaptureTimestamp int64    `json:"captureTimestamp"`
	Comment          string   `json:"comment"`
}

type GetCardResponse struct {
	Id string `json:"id,omitempty"`
	MemberBasicInfo
	Error string `json:"error,omitempty"`
}

type GetDetailResponse struct {
	Id     string           `json:"id,omitempty"`
	Basic  MemberBasicInfo  `json:"basic,omitempty"`
	Detail MemberDetailInfo `json:"detail,omitempty"`
	Other  memberOtherInfo  `json:"other,omitempty"`
	Error  string           `json:"error,omitempty"`
}

func makeCardEndpoint(ms MemberService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getCardRequest)
		member, err := ms.Card(ctx, req.Id)
		if err != nil {
			return GetCardResponse{Error: err.Error()}, nil
		}
		member.Watched, err = ms.GetMemberWatchedStatus(ctx, req.Id, req.UserId)
		if err != nil {
			return GetDetailResponse{Error: err.Error()}, nil
		}
		basic := memberToBasicInfo(member)
		return GetCardResponse{MemberBasicInfo: basic, Id: member.Id}, nil
	}
}

func makeDetailEndpoint(ms MemberService, cus CollectorUserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getDetailRequest)
		member, err := ms.Detail(ctx, req.Id)
		if err != nil {
			return GetDetailResponse{Error: err.Error()}, nil
		}
		member.Watched, err = ms.GetMemberWatchedStatus(ctx, req.Id, req.UserId)
		if err != nil {
			return GetDetailResponse{Error: err.Error()}, nil
		}
		// 将member转换为basic和detail
		basic := memberToBasicInfo(member)
		detail := memberToDetailInfo(member)
		var users []CollectorUser
		for _, collector := range member.Collectors {
			// 通过CollectorUserService获取每个采集器关联的采集用户
			us, _ := cus.Users(ctx, collector)
			if len(us) != 0 {
				users = append(users, us...)
			}
		}
		// 将member和采集用户组织成otherInfo格式
		o := memberCollectorUserToMemberOtherInfo(member, users)
		other := memberOtherInfoFormatter(o)
		// 序列化为response返回
		return GetDetailResponse{
			Id:     member.Id,
			Basic:  basic,
			Detail: detail,
			Other:  other}, nil
	}
}

// makeUpdateCardEndpoint是更新人卡片的endpoint
// todo 此处watched是重点关注功能应该由重点关注模块提供，此处不应该提供编辑接口
func makeUpdateCardEndpoint(ms MemberService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(updateCardRequest)
		// 获取member对象用于判断id是否存在；此外在保存时可以附带历史未编辑的数据
		member, err := ms.Card(ctx, req.Id)
		if err != nil {
			return updateCardResponse{Error: err.Error()}, nil
		}
		// 将可编辑的参数进行设置
		member.Comment = req.Comment
		member.Tags = req.Tags
		// 调用memberService进行更新
		_, err = ms.UpdateMember(ctx, member)
		if err != nil {
			return updateCardResponse{Error: err.Error()}, nil
		}
		return updateCardResponse{Id: member.Id}, nil
	}
}

// makeUpdateDetailEndpoint是更新人详情的endpoint
func makeUpdateMemberEndpoint(ms MemberService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(updateDetailRequest)
		// 获取人详情的member对象
		member, err := ms.Detail(ctx, req.Id)
		if err != nil {
			return updateDetailResponse{Error: err.Error()}, nil
		}
		// 将所有可编辑的字段进行设置
		member.Tags = req.Basic.Tags
		member.Name = req.Detail.Name
		member.Age = req.Detail.Age
		member.Sex = req.Detail.Sex
		member.PhoneNumber = strings.Fields(req.Detail.Phones)
		member.IdCard = req.Detail.IdCard
		member.CensusAddress = req.Detail.CensusAddress
		member.ResidenceAddress = req.Detail.ResidenceAddress
		// 调用memberService进行更新
		_, err = ms.UpdateMember(ctx, member)
		if err != nil {
			return updateDetailResponse{Error: err.Error()}, nil
		}
		return updateDetailResponse{Id: member.Id}, nil
	}
}
