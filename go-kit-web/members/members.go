/*
	members模块提供网民相关编辑和查看的接口和服务
*/
package members

import (
	"dash/collectors"
	"dash/messages"
	"strings"
)

// todo 没有数据要不要默认返回空值
type chatroom struct {
	Id            string `json:"id,omitempty"`
	Name          string `json:"name,omitempty"`
	OwnerNickname string `json:"ownerNickname,omitempty"`
	OwnerId       string `json:"ownerId,omitempty"`
	Alias         string `json:"alias,omitempty"`
	MessageCount7 int64  `json:"messageCount7,omitempty"`
}

type CollectorUser struct {
	Id           string
	Name         string
	Organization organization
}

type organization struct {
	Name string
}

type MemberBasicInfo struct {
	Age                  int64      `json:"age"`
	DataSource           string     `json:"dataSource"`
	Nickname             string     `json:"nickname"`
	UsedNickname         []string   `json:"usedNickname"`
	Address              string     `json:"address"`
	Avatar               string     `json:"avatar"`
	Sex                  string     `json:"sex"`
	Signature            string     `json:"signature"`
	UsedSignature        []string   `json:"usedSignature"`
	Tags                 []string   `json:"tags"`
	LastMessageTimestamp int64      `json:"lastMessageTimestamp"`
	MessageCount7        int64      `json:"messageCount7"`
	AlarmCount7          int64      `json:"alarmCount7"`
	Chatrooms            []chatroom `json:"chatrooms"`
	Comment              string     `json:"comment"`
	Watched              bool       `json:"watched"`
	CaptureTimestamp     int64      `json:"-"`
}

type MemberDetailInfo struct {
	Name             string `json:"name"`
	Age              int64  `json:"age"`
	Sex              string `json:"sex"`
	Phones           string `json:"phones"`
	IdCard           string `json:"idCard"`
	CensusAddress    string `json:"censusAddress"`
	ResidenceAddress string `json:"residenceAddress"`
}

// 区别于memberOtherInfo结构体，此结构体用于service中获取到的数据序列化
// 之后返回标准的格式需要转换为memberOtherInfo
type MemberOtherInfo struct {
	CollectorOrgans  []organization
	Users            []CollectorUser
	Collectors       []collectors.Collector
	Comment          string
	CaptureTimestamp int64
}

func memberToBasicInfo(member messages.Member) MemberBasicInfo {
	basic := MemberBasicInfo{
		Age:                  member.Age,
		Watched:              member.Watched,
		DataSource:           member.DataSource,
		Nickname:             member.NickName,
		UsedNickname:         member.UsedNickname,
		Avatar:               member.Avatar,
		Sex:                  member.Sex,
		Signature:            member.Signature,
		UsedSignature:        member.UsedSignature,
		Tags:                 member.Tags,
		LastMessageTimestamp: member.LastMessageTimestamp,
		MessageCount7:        member.MessageCount7,
		AlarmCount7:          member.AlarmCount7,
		Comment:              member.Comment,
		CaptureTimestamp:     member.CaptureTimestamp,
		Address:              strings.Join(member.Address, " "),
	}
	var chatrooms []chatroom
	for i, c := range member.Chatrooms {
		cr := chatroom{
			Id:            c.Id,
			Name:          c.Name,
			OwnerNickname: c.Owner.NickName,
			OwnerId:       c.Owner.Id,
			Alias:         member.Aliases[i],
			MessageCount7: c.MessageCount,
		}
		chatrooms = append(chatrooms, cr)
	}
	basic.Chatrooms = chatrooms
	return basic
}

func memberToDetailInfo(member messages.Member) MemberDetailInfo {
	return MemberDetailInfo{
		Name:             member.Name,
		Age:              member.Age,
		Sex:              member.Sex,
		Phones:           strings.Join(member.PhoneNumber, " "),
		IdCard:           member.IdCard,
		CensusAddress:    member.CensusAddress,
		ResidenceAddress: member.ResidenceAddress,
	}
}

func memberCollectorUserToMemberOtherInfo(member messages.Member, users []CollectorUser) MemberOtherInfo {
	var organs []organization
	keys := make(map[string]bool)
	for _, user := range users {
		if _, ok := keys[user.Organization.Name]; !ok{
			keys[user.Organization.Name] = true
			organs = append(organs, user.Organization)
		}
	}
	return MemberOtherInfo{
		CollectorOrgans:  organs,
		Users:            users,
		Collectors:       member.Collectors,
		Comment:          member.Comment,
		CaptureTimestamp: member.CaptureTimestamp,
	}
}

func memberOtherInfoFormatter(other MemberOtherInfo) memberOtherInfo {
	o := memberOtherInfo{CaptureTimestamp: other.CaptureTimestamp, Comment: other.Comment}
	var users []string
	var collectorIds []string
	var collectorOrgans []string
	for _, user := range other.Users {
		users = append(users, user.Organization.Name+"——"+user.Name+" ("+user.Id+")")
	}
	for _, collector := range other.Collectors {
		collectorIds = append(collectorIds, collector.Id)
	}
	for _, organ := range other.CollectorOrgans{
		collectorOrgans = append(collectorOrgans, organ.Name)
	}
	o.Users = users
	o.Collectors = collectorIds
	o.CollectorOrgans = collectorOrgans
	return o
}
