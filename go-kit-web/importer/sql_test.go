package importer

import (
	"context"
	"dash/collectors"
	"database/sql"
	"github.com/go-kit/kit/log"
	_ "github.com/go-sql-driver/mysql"
	"os"
	"testing"
)

func TestSaveMember(t *testing.T) {
	db, err := sql.Open("mysql", "root:nil@tcp(127.0.0.1:3306)/dash")
	if err != nil {
		panic(err)
	}
	logger := log.NewLogfmtLogger(os.Stderr)
	repo := repository{
		logger:  logger,
		mysqlDB: db,
	}
	member := collectors.Member{
		Id:         "002",
		Nickname:   "003",
		Avatar:     "",
		Signature:  "",
		Address:    []string{"abc"},
		Sex:        "女",
		DataSource: "",
	}
	err = repo.saveMember(context.Background(), []collectors.Member{member})
	if err != nil {
		t.Error("err", err.Error())
	}
}
