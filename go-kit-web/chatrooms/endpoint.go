package chatrooms

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type ChatroomRequest struct {
	Id     string `json:"id,omitempty"`
	UserId string `json:"userId"`
}

type ChatroomCardResponse struct {
	Id           string            `json:"id"`
	Name         string            `json:"name"`
	Owner        *MemberResponse   `json:"owner"`
	Avatar       string            `json:"avatar"`
	User         []string          `json:"user"`
	DataSource   string            `json:"dataSource"`
	MessageCount int64             `json:"messageCount"`
	Watched      bool              `json:"watched"`
	Tags         []string          `json:"tags"`
	Category     []string          `json:"category"`
	Comment      string            `json:"comment"`
	UsedName     []string          `json:"usedName"`
	Members      []*MemberResponse `json:"members"`
	*ChatroomCount
	Error string `json:"error,omitempty"`
}

type ChatroomDetailResponse struct {
	Id               string            `json:"id"`
	Name             string            `json:"name"`
	Owner            *MemberResponse   `json:"owner"`
	Avatar           string            `json:"avatar"`
	Collectors       []string          `json:"collectors"`
	Users            []string          `json:"users"`
	CollectorOrgans  []string          `json:"collectorOrgans"`
	CaptureTimestamp int64             `json:"captureTimestamp"`
	DataSource       string            `json:"dataSource"`
	Watched          bool              `json:"watched"`
	Tags             []string          `json:"tags"`
	Category         []string          `json:"category"`
	Comment          string            `json:"comment"`
	UsedName         []string          `json:"usedName"`
	Members          []*MemberResponse `json:"members"`
	*ChatroomCount
	Error string `json:"error,omitempty"`
}

type MemberResponse struct {
	Id           string          `json:"id"`
	NickName     string          `json:"nickname"`
	Alias        string          `json:"alias"`
	JoinMethod   string          `json:"joinMethod"`
	Inviter      *MemberResponse `json:"inviter"`
	MessageCount int64           `json:"messageCount"`
}

func makeChatroomCardEndpoint(svc ChatroomService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(ChatroomRequest)
		card, err := svc.Card(ctx, req.Id, req.UserId)
		if err != nil {
			return ChatroomCardResponse{Error: err.Error()}, nil
		}
		return card, nil
	}
}

func makeChatroomDetailEndpoint(svc ChatroomService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(ChatroomRequest)
		detail, err := svc.Detail(ctx, req.Id, req.UserId)
		if err != nil {
			return ChatroomDetailResponse{Error: err.Error()}, nil
		}
		return detail, nil
	}
}

type UpdateCommentRequest struct {
	Id      string `json:"id,omitempty"`
	Comment string `json:"comment"`
}

type UpdateTagsRequest struct {
	Id   string   `json:"id,omitempty"`
	Tags []string `json:"tags"`
}

type UpdateCategoryRequest struct {
	Id         string `json:"id,omitempty"`
	CategoryId string `json:"categoryId"`
}

type UpdateWatchedRequest struct {
	Id      string `json:"id,omitempty"`
	Watched bool   `json:"watched"`
}

type UpdateCategoryResponse struct {
	CategoryIds []string `json:"categoryIds,omitempty"`
	Error       string   `json:"error,omitempty"`
}

type UpdateResponse struct {
	Error string `json:"error,omitempty"`
}

func makeUpdateCommentEndpoint(svc ChatroomService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(UpdateCommentRequest)
		err = svc.UpdateComment(context.Background(), req.Id, req.Comment)
		if err != nil {
			return UpdateResponse{err.Error()}, nil
		}
		return UpdateResponse{}, nil
	}
}

func makeUpdateTagsEndpoint(svc ChatroomService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(UpdateTagsRequest)
		err = svc.UpdateTags(context.Background(), req.Id, req.Tags)
		if err != nil {
			return UpdateResponse{err.Error()}, nil
		}
		return UpdateResponse{}, nil
	}
}

func makeUpdateCategoryEndpoint(svc ChatroomService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(UpdateCategoryRequest)
		categoryIds, err := svc.UpdateCategory(context.Background(), req.Id, req.CategoryId)
		if err != nil {
			return UpdateCategoryResponse{Error: err.Error()}, nil
		}
		return UpdateCategoryResponse{CategoryIds: categoryIds}, nil
	}
}

func makeUpdateWatchedEndpoint(svc ChatroomService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(UpdateWatchedRequest)
		err = svc.UpdateWatched(context.Background(), req.Id, req.Watched)
		if err != nil {
			return UpdateResponse{err.Error()}, nil
		}
		return UpdateResponse{}, nil
	}
}
