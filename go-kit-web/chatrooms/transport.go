package chatrooms

import (
	"context"
	"dash/jwt"
	"encoding/json"
	"errors"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
)

func MakeHandler(svc ChatroomService) http.Handler {
	cardHandler := kithttp.NewServer(
		makeChatroomCardEndpoint(svc),
		decodeChatroomRequest,
		kithttp.EncodeJSONResponse)
	detailHandler := kithttp.NewServer(
		makeChatroomDetailEndpoint(svc),
		decodeChatroomRequest,
		kithttp.EncodeJSONResponse)
	commentHandler := kithttp.NewServer(
		makeUpdateCommentEndpoint(svc),
		decodeCommentRequest,
		kithttp.EncodeJSONResponse)
	tagsHandler := kithttp.NewServer(
		makeUpdateTagsEndpoint(svc),
		decodeTagsRequest,
		kithttp.EncodeJSONResponse)
	categoryHandler := kithttp.NewServer(
		makeUpdateCategoryEndpoint(svc),
		decodeCategoryRequest,
		kithttp.EncodeJSONResponse)
	watchedHandler := kithttp.NewServer(
		makeUpdateWatchedEndpoint(svc),
		decodeWatchedRequest,
		kithttp.EncodeJSONResponse)
	r := mux.NewRouter()
	r.Handle("/api/v1/chatroom/card", cardHandler).Methods("GET")
	r.Handle("/api/v1/chatroom/detail", detailHandler).Methods("GET")
	r.Handle("/api/v1/chatroom/comment", commentHandler).Methods("POST")
	r.Handle("/api/v1/chatroom/tags", tagsHandler).Methods("POST")
	r.Handle("/api/v1/chatroom/category", categoryHandler).Methods("POST")
	r.Handle("/api/v1/chatroom/watched", watchedHandler).Methods("POST")
	return r
}

func decodeChatroomRequest(_ context.Context, r *http.Request) (interface{}, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.New("参数不正确")
	}
	id := r.Form.Get("id")
	user := r.Context().Value("user").(*jwt.User)
	return ChatroomRequest{Id: id, UserId: user.Id}, nil
}

func decodeCommentRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Id      string `json:"id,omitempty"`
		Comment string `json:"comment"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, errors.New("参数不正确")
	}
	return UpdateCommentRequest{
		Id:      body.Id,
		Comment: body.Comment,
	}, nil
}

func decodeTagsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Id   string   `json:"id,omitempty"`
		Tags []string `json:"tags"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, errors.New("参数不正确")
	}
	return UpdateTagsRequest{
		Id:   body.Id,
		Tags: body.Tags,
	}, nil
}

func decodeCategoryRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Id         string `json:"id,omitempty"`
		CategoryId string `json:"categoryId"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, errors.New("参数不正确")
	}
	return UpdateCategoryRequest{
		Id:         body.Id,
		CategoryId: body.CategoryId,
	}, nil
}

func decodeWatchedRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body struct {
		Id      string `json:"id,omitempty"`
		Watched bool   `json:"watched"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return nil, errors.New("参数不正确")
	}
	return UpdateWatchedRequest{
		Id:      body.Id,
		Watched: body.Watched,
	}, nil
}
