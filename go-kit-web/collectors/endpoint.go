package collectors

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

func makeReportMessageEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(MessageReport)
		err := svc.ReportMessage(ctx, req)
		if err != nil {
			return MessageReportResponse{Error: err.Error()}, nil
		}
		return MessageReportResponse{}, nil
	}
}

func makeReportUploadEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UploadReport)
		err := svc.ReportUpload(ctx, req)
		if err != nil {
			return GenericResponse{Error: err.Error()}, nil
		}
		return GenericResponse{}, nil
	}
}

func makeReportCollectorJoinChatroomEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CollectorJoinChatroomReport)
		err := svc.ReportCollectorJoinChatroom(ctx, req)
		if err != nil {
			return GenericResponse{Error: err.Error()}, nil
		}
		return GenericResponse{}, nil
	}
}

func makeReportCollectorLeaveChatroomEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CollectorLeaveChatroomReport)
		err := svc.ReportCollectorLeaveChatroom(ctx, req)
		if err != nil {
			return GenericResponse{Error: err.Error()}, err
		}
		return GenericResponse{}, nil
	}
}

func makeReportMemberJoinChatroomEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(MemberJoinChatroomReport)
		err := svc.ReportMemberJoinChatroom(ctx, req)
		if err != nil {
			return GenericResponse{Error: err.Error()}, nil
		}
		return GenericResponse{}, nil
	}
}

func makeReportMemberLeaveChatroomEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(MemberLeaveChatroomReport)
		err := svc.ReportMemberLeaveChatroom(ctx, req)
		if err != nil {
			return GenericResponse{Error: err.Error()}, nil
		}
		return GenericResponse{}, nil
	}
}

func makeReportChatroomMemberEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ChatroomMemberReport)
		err := svc.ReportChatroomMember(ctx, req)
		if err != nil {
			return GenericResponse{Error: err.Error()}, nil
		}
		return GenericResponse{}, nil
	}
}

func makeReportLogEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(LogReport)
		err := svc.ReportLog(ctx, req)
		if err != nil {
			return GenericResponse{Error: err.Error()}, nil
		}
		return GenericResponse{}, nil
	}
}

func makeReportHeartBeatEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(HeartBeatReport)
		err := svc.ReportHeartbeat(ctx, req)
		if err != nil {
			return GenericResponse{Error: err.Error()}, nil
		}
		return GenericResponse{}, nil
	}
}
