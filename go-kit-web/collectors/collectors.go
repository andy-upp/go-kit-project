package collectors

import "io"

type Collector struct {
	Id   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type Member struct {
	Id         string   `json:"id" validate:"required"`
	Nickname   string   `json:"nickname"`
	Avatar     string   `json:"avatar"`
	Signature  string   `json:"signature"`
	Address    []string `json:"address,omitempty"`
	Sex        string   `json:"sex"`
	DataSource string   `json:"dataSource"`
}

type Chatroom struct {
	Id               string   `json:"id" validate:"required"`
	Name             string   `json:"name"`
	Avatar           string   `json:"avatar"`
	Owner            Member   `json:"owner"`
	Members          []Member `json:"members,omitempty" validate:"dive,required"`
	DataSource       string   `json:"dataSource"`
	CaptureTimestamp int64    `json:"captureTimestamp"`
}

type MessageReport struct {
	Id               string   `json:"id" validate:"required"`
	Content          string   `json:"content"`
	Type             string   `json:"type" validate:"required"`
	Filename         string   `json:"filename"`
	FilePath         string   `json:"filePath"`
	FileMd5          string   `json:"fileMd5"`
	FileSize         int64    `json:"fileSize"`
	Format           string   `json:"format"`
	CollectorId      string   `json:"collectorId" validate:"required"`
	CaptureTimestamp int64    `json:"captureTimestamp"`
	Timestamp        int64    `json:"timestamp"`
	DataSource       string   `json:"dataSource"`
	Member           Member   `json:"member"`
	Chatroom         Chatroom `json:"chatroom"`
}

type UploadReport struct {
	MessageReport MessageReport
	File          io.ReadCloser
}

type CollectorJoinChatroomReport struct {
	CollectorId      string   `json:"collectorId" validate:"required"`
	CaptureTimestamp int64    `json:"captureTimestamp"`
	Timestamp        int64    `json:"timestamp"`
	Chatroom         Chatroom `json:"chatroom" validate:"required"`
}

type CollectorLeaveChatroomReport struct {
	CollectorId      string   `json:"collectorId" validate:"required"`
	CaptureTimestamp int64    `json:"captureTimestamp"`
	Timestamp        int64    `json:"timestamp"`
	Chatroom         Chatroom `json:"chatroom" validate:"required"`
}

type MemberJoinChatroomReport struct {
	CollectorId      string   `json:"collectorId" validate:"required"`
	CaptureTimestamp int64    `json:"captureTimestamp"`
	Timestamp        int64    `json:"timestamp"`
	Member           Member   `json:"member" validate:"required"`
	JoinMethod       string   `json:"joinMethod" validate:"required"`
	Inviter          *Member  `json:"inviter,omitempty"`
	Chatroom         Chatroom `json:"chatroom" validate:"required"`
}

type MemberLeaveChatroomReport struct {
	CollectorId      string   `json:"collectorId" validate:"required"`
	CaptureTimestamp int64    `json:"captureTimestamp"`
	Timestamp        int64    `json:"timestamp"`
	Member           Member   `json:"member" validate:"required"`
	Chatroom         Chatroom `json:"chatroom" validate:"required"`
}

type ChatroomMemberReport struct {
	CollectorId      string     `json:"collectorId" validate:"required"`
	CaptureTimestamp int64      `json:"captureTimestamp"`
	Chatrooms        []Chatroom `json:"chatrooms,omitempty"`
}

type LogReport struct {
	CollectorId      string                 `json:"collectorId" validate:"required"`
	CaptureTimestamp int64                  `json:"captureTimestamp"`
	Timestamp        int64                  `json:"timestamp"`
	Log              map[string]interface{} `json:"log,omitempty"`
}

type HeartBeatReport struct {
	CollectorId string `json:"collectorId" validate:"required"`
	Timestamp   int64  `json:"timestamp"`
}

type MessageReportResponse struct {
	Error string `json:"error,omitempty"`
}

type GenericResponse struct {
	Error string `json:"error,omitempty"`
}
