package categories

//该文件目前没用了 20200628
import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type createCategoryRequest struct {
	Id              string `json:"id"`
	Name            string `json:"name"`
	ParentId        string `json:"parentId"`
	CreateTimestamp int64  `json:"createTimestamp"`
}

type createCategoryResponse struct {
	Error string `json:"error,omitempty"`
}

type getCategoriesRequest struct {
}

type getCategoriesResponse struct {
	Data  []*CategoryResponse `json:"data,omitempty"`
	Error string              `json:"error,omitempty"`
}

func makeCreateCategoryEndpoint(cs CategoryService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(createCategoryRequest)
		err = cs.CreateCategory(context.Background(), req.Id, req.Name, req.CreateTimestamp, req.ParentId)
		if err == nil {
			return createCategoryResponse{}, nil
		}
		return createCategoryResponse{Error: err.Error()}, nil
	}
}

func makeGetCategoriesEndpoint(cs CategoryService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		_ = request.(getCategoriesRequest)
		cats, err := cs.GetCategories(context.Background())
		if err == nil {
			return getCategoriesResponse{Data: cats}, nil
		}
		return getCategoriesResponse{Data: cats, Error: err.Error()}, nil
	}
}
