package categories

import "errors"

type Category struct {
	Id              string      `json:"id,omitempty"`
	Name            string      `json:"name,omitempty"`
	ParentId        string      `json:"parentId,omitempty"`
	CreateTimestamp int64       `json:"createTimestamp,omitempty"`
	Parent          *Category   `json:"-"`
	Children        []*Category `json:"children,omitempty"`
}

type CategoryResponse struct {
	Category
	ParentName   []string            `json:"parentName,omitempty"`
	ChildrenResp []*CategoryResponse `json:"children,omitempty"`
}

func (o *Category) ParentIds() ([]string, error) {
	if o.Parent == nil {
		return nil, nil
	}
	parentIds, err := o.Parent.ParentIds()
	if err != nil {
		return nil, err
	}
	for _, parentId := range parentIds {
		if parentId == o.Parent.Id {
			return nil, errors.New("circular dependency")
		}
	}
	return append([]string{o.Parent.Id}, parentIds...), nil
}
