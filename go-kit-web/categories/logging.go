package categories

import (
	"github.com/go-kit/kit/log"
	"os"
)

func NewLogger() log.Logger {
	var logger log.Logger
	logger = log.NewJSONLogger(os.Stdout)
	logger = log.With(logger, "time", log.DefaultTimestampUTC)
	logger = log.With(logger, "caller", log.DefaultCaller)
	return logger
}
