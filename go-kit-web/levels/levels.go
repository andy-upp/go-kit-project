package levels

type Level struct {
	Id       string `json:"id"`
	Name     string `json:"name"`
	ParentId string `json:"parentId"`
	L        string `json:"l"`
}
