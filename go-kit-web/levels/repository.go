package levels

import "database/sql"

type mysqlUserLevelRow struct {
	Id    sql.NullString
	PId   sql.NullString
	Name  sql.NullString
	Level sql.NullString
}
