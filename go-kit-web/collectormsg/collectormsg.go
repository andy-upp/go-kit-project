package collectormsg

type CollectorOrganization struct {
	Id       string                   `json:"id,omitempty"`
	Name     string                   `json:"name,omitempty"`
	ParentId string                   `json:"parentId,omitempty"`
	Children []*CollectorOrganization `json:"children,omitempty"`
}

type Collector struct {
	Id   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type topQueryData map[string]interface{}

type Tree struct {
	Nodes    map[string]*CollectorOrganization
	TopNodes []*CollectorOrganization
}

//type Node struct {
//	Id    string     `json:"id"`
//	Pid   string     `json:"pid"`
//	Name  string  `json:"name"`
//	Children []*Node `json:"children"`
//}

// 平铺数据创建树结构
func (t *Tree) GenerateTree(nodes []*CollectorOrganization) {
	t.Nodes = make(map[string]*CollectorOrganization, 0)
	t.NewNodes(nodes)
	for _, node := range nodes {
		if parentNode, ok := t.Nodes[node.ParentId]; ok {
			parentNode.Children = append(parentNode.Children, node)
		} else {
			t.TopNodes = append(t.TopNodes, node)
		}
	}
}

func (t *Tree) NewNodes(nodes []*CollectorOrganization) {
	for _, CollectorOrganization := range nodes {
		t.Nodes[CollectorOrganization.Id] = CollectorOrganization
	}
}

func (t *Tree) SearchNodeById(id string) *CollectorOrganization {
	if CollectorOrganization, ok := t.Nodes[id]; ok {
		return CollectorOrganization
	} else {
		return nil
	}
}
