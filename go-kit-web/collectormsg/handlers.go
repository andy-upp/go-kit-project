package collectormsg

import (
	"context"
	"dash/categories"
	"dash/topics"
	"github.com/go-kit/kit/log"
)

type TopQueryGroupHandlers struct {
	logger   log.Logger
	handlers map[string]QueryHandler
}

func NewQueryGroupHandlers(logger log.Logger) TopQueryGroupHandlers {
	return TopQueryGroupHandlers{
		logger:   logger,
		handlers: map[string]QueryHandler{},
	}
}

func (h *TopQueryGroupHandlers) AddHandler(query string, handler QueryHandler) {
	h.handlers[query] = handler
}

type QueryHandler func(ctx context.Context) (interface{}, error)

func NewDataSourceHandler() QueryHandler {
	return func(ctx context.Context) (interface{}, error) {
		dataSource := []string{"微信"}
		return dataSource, nil
	}
}

func NewCategoryHandler(categoryService categories.CategoryService) QueryHandler {
	return func(ctx context.Context) (interface{}, error) {
		return categoryService.GetCategories(ctx)
	}
}

func NewSmartTopicsHandler(smartTopicService topics.SmartTopicsService) QueryHandler {
	return func(ctx context.Context) (interface{}, error) {
		return smartTopicService.Get(ctx), nil
	}
}

func NewKeyworsTopicHandler(keywordService topics.KeywordTopicTreeService) QueryHandler {
	return func(ctx context.Context) (interface{}, error) {
		return keywordService.Get(ctx)
	}
}

func NewWorkingAddressHandler(organizationTree OrganizationService) QueryHandler {
	return func(ctx context.Context) (interface{}, error) {
		return organizationTree.GetOrganizationTree(ctx, "")
	}
}
