package collectormsg

import (
	"context"
	"dash/users"
	"errors"
	"github.com/go-kit/kit/log"
)

type OrganizationService interface {
	GetUsers(ctx context.Context, id string) ([]*users.User, error)
	GetOrganizationTree(ctx context.Context, id string) ([]*CollectorOrganization, error)
}

type defaultOrganizationService struct {
	repo   OrganizationRepository
	logger log.Logger
}

type CollectorService interface {
	GetCollector(ctx context.Context, organizationId string, userId string) ([]*Collector, error)
}

type defaultCollectorService struct {
	repo   OrganizationRepository
	logger log.Logger
}

func NewCollectorService(repo OrganizationRepository, logger log.Logger) CollectorService {
	return &defaultCollectorService{
		repo:   repo,
		logger: logger,
	}
}

func NewOrganizationService(repo OrganizationRepository, logger log.Logger) OrganizationService {
	return &defaultOrganizationService{
		repo:   repo,
		logger: logger,
	}
}

func (dcs *defaultCollectorService) GetCollector(ctx context.Context, organizationId string, userId string) ([]*Collector, error) {
	var collectorIds []string
	if userId != "" {
		collectorIds = append(collectorIds, userId)
	} else if organizationId != "" {
		var userCollectorIds []string
		userRes, err := dcs.repo.GetAllUser(ctx, organizationId)
		if err != nil {
			return nil, err
		}
		for _, user := range userRes {
			userCollectorIds = append(userCollectorIds, *user.Id)
		}
		collectorIds = append(collectorIds, userCollectorIds...)
	} else {
		return nil, errors.New("参数不正确")
	}
	collectorRes, err := dcs.repo.GetCollector(ctx, collectorIds)
	if err != nil {
		return nil, err
	}
	return collectorRes, nil
}

func (dos *defaultOrganizationService) GetUsers(ctx context.Context, id string) ([]*users.User, error) {
	usersResult, err := dos.repo.GetAllUser(ctx, id)
	if err != nil {
		return nil, err
	}
	return usersResult, nil
}

func (dos *defaultOrganizationService) GetOrganizationTree(ctx context.Context, id string) ([]*CollectorOrganization, error) {
	organRes, err := dos.repo.GetOrganizationTree(ctx)
	var organTree Tree
	organTree.GenerateTree(organRes)
	if err != nil {
		return nil, err
	}
	return organTree.TopNodes, nil
}

type TopParamsService interface {
	TopSearchParameters(ctx context.Context, querys []string) (interface{}, error)
}

type topParamsService struct {
	groupHandlers TopQueryGroupHandlers
	logger        log.Logger
}

func (svc *topParamsService) TopSearchParameters(ctx context.Context, querys []string) (interface{}, error) {
	response := topQueryData{}
	if len(querys) <= 0 {
		return nil, errors.New("请输入正确的参数")
	}
	for _, query := range querys {
		handler, exists := svc.groupHandlers.handlers[query]
		if !exists {
			continue
		}
		result, err := handler(ctx)
		if err != nil {
			continue
		}
		response[query] = result
	}
	return response, nil
}

func NewTopParamsService(topGroupHandlers TopQueryGroupHandlers, logger log.Logger) TopParamsService {
	return &topParamsService{
		groupHandlers: topGroupHandlers,
		logger:        logger,
	}
}
