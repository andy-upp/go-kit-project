package collectormsg

import (
	"context"
	"dash/users"
	"github.com/go-kit/kit/endpoint"
)

type getOrganizationUsersRequest struct {
	Id string `json:"id"`
}

type getOrganizationUsersResponse struct {
	Data  []*users.User `json:"data,omitempty"`
	Error string        `json:"error,omitempty"`
}

type getCollectorRequest struct {
	UserId         string `json:"userId"`
	OrganizationId string `json:"organizationId"`
}

type getCollectorResponse struct {
	Data  []*Collector `json:"data,omitempty"`
	Error string       `json:"error,omitempty"`
}

func makeGetCollectorEndpoint(svc CollectorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getCollectorRequest)
		collectorRes, err := svc.GetCollector(ctx, req.OrganizationId, req.UserId)
		if err == nil {
			return getCollectorResponse{Data: collectorRes}, nil
		}
		return getCollectorResponse{Error: err.Error()}, nil
	}
}

func makeGetOrganizationEndpoint(svc OrganizationService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getOrganizationUsersRequest)
		userRes, err := svc.GetUsers(ctx, req.Id)
		if err == nil {
			return getOrganizationUsersResponse{Data: userRes}, nil
		}
		return getOrganizationUsersResponse{Error: err.Error()}, nil
	}
}

func makeTopParamsEndpoint(svc TopParamsService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.([]string)
		queryResult, err := svc.TopSearchParameters(ctx, req)
		if err != nil {
			return map[string]string{"error": err.Error()}, nil
		}
		return queryResult, nil
	}
}
