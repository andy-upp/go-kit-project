package watch

import (
	"context"
	"dash/jwt"
	"encoding/json"
	"errors"
	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
)

func MakeHandler(ws WatchService, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		kithttp.ServerErrorEncoder(errorEncoder),
	}

	r := mux.NewRouter()
	e := makeServerEndpoints(ws)

	searchWatchedMemberHandler := kithttp.NewServer(
		e.SearchWatchedMemberEndpoint,
		decodeSearchWatchedMemberRequest,
		encodeSearchWatchedMemberResponse,
		opts...,
	)

	searchWatchedChatroomHandler := kithttp.NewServer(
		e.SearchWatchedChatroomEndpoint,
		decodeSearchWatchedChatroomRequest,
		encodeSearchWatchedChatroomResponse,
		opts...,
	)

	modifyWatchedMemberHandler := kithttp.NewServer(
		e.ModifyWatchedMemberEndpoint,
		decodeModifyWatchedMemberRequest,
		encodeModifyWatchedMemberResponse,
		opts...,
	)

	modifyWatchedChatroomHandler := kithttp.NewServer(
		e.ModifyWatchedChatroomEndpoint,
		decodeModifyWatchedChatroomRequest,
		encodeModifyWatchedChatroomResponse,
		opts...,
	)

	r.Handle("/api/v1/watch/member/search", searchWatchedMemberHandler).Methods("POST")
	r.Handle("/api/v1/watch/chatroom/search", searchWatchedChatroomHandler).Methods("POST")
	r.Handle("/api/v1/watch/member", modifyWatchedMemberHandler).Methods("POST")
	r.Handle("/api/v1/watch/chatroom", modifyWatchedChatroomHandler).Methods("POST")
	return r
}

func decodeSearchWatchedMemberRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var request searchWatchedMemberRequest
	user := r.Context().Value("user").(*jwt.User)
	request.UserId = user.Id
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeSearchWatchedMemberResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func decodeSearchWatchedChatroomRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var request searchWatchedChatroomRequest
	u := r.Context().Value("user")
	if u == nil {
		return nil, errors.New("user notfound")
	}
	request.UserId = u.(*jwt.User).Id
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeSearchWatchedChatroomResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func errorEncoder(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	_ = json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
}

func decodeModifyWatchedChatroomRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var request modifyWatchedChatroomRequest
	u := r.Context().Value("user")
	if u == nil {
		return nil, errors.New("user notfound")
	}
	request.UserId = u.(*jwt.User).Id
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeModifyWatchedChatroomResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func decodeModifyWatchedMemberRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var request modifyWatchedMemberRequest
	u := r.Context().Value("user")
	if u == nil {
		return nil, errors.New("user notfound")
	}
	request.UserId = u.(*jwt.User).Id
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeModifyWatchedMemberResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
