package watch

type WatchedMember struct {
	Id            string `json:"id"`
	Nickname      string `json:"nickname"`
	DataSource    string `json:"dataSource"`
	MessageCount7 int64  `json:"messageCount7"`
	Address       string `json:"address"`
	AddTimestamp  int64  `json:"addTimestamp"`
	Comment       string `json:"comment"`
}

type WatchedChatroom struct {
	Id            string `json:"id"`
	Name          string `json:"name"`
	MemberCount   int64  `json:"memberCount"`
	OwnerNickname string `json:"ownerNickname"`
	OwnerId       string `json:"ownerId"`
	DataSource    string `json:"dataSource"`
	Category      string `json:"category"`
	MessageCount7 int64  `json:"messageCount7"`
	AddTimestamp  int64  `json:"addTimestamp"`
}
