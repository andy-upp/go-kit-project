package watch

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoints struct {
	SearchWatchedChatroomEndpoint endpoint.Endpoint
	SearchWatchedMemberEndpoint   endpoint.Endpoint
	ModifyWatchedChatroomEndpoint endpoint.Endpoint
	ModifyWatchedMemberEndpoint   endpoint.Endpoint
}

func makeServerEndpoints(ws WatchService) Endpoints {
	return Endpoints{
		SearchWatchedChatroomEndpoint: makeSearchWatchedChatRoom(ws),
		SearchWatchedMemberEndpoint:   makeSearchWatchedMember(ws),
		ModifyWatchedChatroomEndpoint: makeModifyWatchedChatroom(ws),
		ModifyWatchedMemberEndpoint:   makeModifyWatchedMember(ws),
	}
}

type searchWatchedMemberRequest struct {
	UserId         string `json:"userId"`
	StartTimestamp int64  `json:"startTimestamp,omitempty"`
	EndTimestamp   int64  `json:"endTimestamp,omitempty"`
	DataSource     string `json:"dataSource,omitempty"`
	Nickname       string `json:"nickname,omitempty"`
	Comment        string `json:"comment,omitempty"`
	Address        string `json:"address,omitempty"`
	CollectorOrgan string `json:"collectorOrgan,omitempty"`
	CollectorUser  string `json:"collectorUser,omitempty"`
	Size           int64  `json:"size,omitempty"`
	Page           int64  `json:"page,omitempty"`
}

type searchWatchedMemberResponse struct {
	Count int64            `json:"count,omitempty"`
	Data  []*WatchedMember `json:"data,omitempty"`
	Error string           `json:"error,omitempty"`
}

type searchWatchedChatroomRequest struct {
	UserId          string `json:"userId,omitempty"`
	DataSource      string `json:"dataSource,omitempty"`
	Name            string `json:"name,omitempty"`
	StartTimestamp  int64  `json:"startTimestamp,omitempty"`
	EndTimestamp    int64  `json:"endTimestamp,omitempty"`
	CategoryId      string `json:"categoryId,omitempty"`
	LowMemberCount  int64  `json:"lowMemberCount,omitempty"`
	HighMemberCount int64  `json:"highMemberCount,omitempty"`
	Size            int64  `json:"size,omitempty"`
	Page            int64  `json:"page,omitempty"`
}

type searchWatchedChatroomResponse struct {
	Count int64              `json:"count,omitempty"`
	Data  []*WatchedChatroom `json:"data,omitempty"`
	Error string             `json:"error,omitempty"`
}

type modifyWatchedMemberRequest struct {
	UserId  string `json:"userId,omitempty"`
	Id      string `json:"id,omitempty"`
	Watched bool   `json:"watched,omitempty"`
}

type modifyWatchedMemberResponse struct {
	Error string `json:"error,omitempty"`
}

type modifyWatchedChatroomRequest struct {
	UserId  string `json:"userId,omitempty"`
	Id      string `json:"id,omitempty"`
	Watched bool   `json:"watched,omitempty"`
}

type modifyWatchedChatroomResponse struct {
	Error string `json:"error,omitempty"`
}

func makeSearchWatchedMember(ws WatchService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(searchWatchedMemberRequest)
		count, data, err := ws.SearchWatchedMember(ctx, req)
		if err != nil {
			return searchWatchedMemberResponse{Error: err.Error()}, err
		}
		return searchWatchedMemberResponse{Data: data, Count: count}, nil
	}
}

func makeSearchWatchedChatRoom(ws WatchService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(searchWatchedChatroomRequest)
		count, data, err := ws.SearchWatchedChatroom(ctx, req)
		if err != nil {
			return nil, err
		}
		return searchWatchedChatroomResponse{
			Count: count,
			Data:  data,
		}, nil
	}
}

func makeModifyWatchedMember(ws WatchService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(modifyWatchedMemberRequest)
		err = ws.ModifyWatchedMember(ctx, req)
		if err != nil {
			return modifyWatchedMemberResponse{Error: err.Error()}, nil
		}
		return modifyWatchedMemberResponse{}, nil
	}
}

func makeModifyWatchedChatroom(ws WatchService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(modifyWatchedChatroomRequest)
		err = ws.ModifyWatchedChatroom(ctx, req)
		if err != nil {
			return modifyWatchedChatroomResponse{Error: err.Error()}, nil
		}
		return modifyWatchedChatroomResponse{}, nil
	}
}
