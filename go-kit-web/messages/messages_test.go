package messages

import (
	"context"
	"database/sql"
	"fmt"
	kitlog "github.com/go-kit/kit/log"
	_ "github.com/go-sql-driver/mysql"
	"github.com/olivere/elastic/v7"
	"github.com/stretchr/testify/suite"
	"log"
	"os"
	"testing"
)

type MessageSuite struct {
	suite.Suite
	MessageSvc Service
}

func NewLogger() kitlog.Logger {
	var logger kitlog.Logger
	logger = kitlog.NewJSONLogger(os.Stdout)
	logger = kitlog.With(logger, "time", kitlog.DefaultTimestampUTC)
	logger = kitlog.With(logger, "caller", kitlog.DefaultCaller)
	return logger
}

func TestMessageSearch(t *testing.T) {
	esClient, err := elastic.NewSimpleClient(elastic.SetURL("http://192.168.31.36:19200"))
	if err != nil {
		log.Fatal(err)
	}
	mysqlClient, err := sql.Open("mysql", "root:nil@tcp(192.168.31.36:13306)/dash")
	logger := NewLogger()
	mr, err := NewMessageRepository(esClient, logger)
	fr, err := NewFileRepository(mysqlClient, esClient, logger)
	fileDir := "/file/"
	ms := NewService(mr, fr, logger, fileDir)
	MessageSuite := &MessageSuite{
		MessageSvc: ms,
	}
	suite.Run(t, MessageSuite)
}

//func (suite *MessageSuite) TestMessageSearch() {
//	//var req messageSearchRequest
//	count, data, err := suite.MessageSvc.MessageSearch(context.Background(), nil)
//	suite.Require().Nil(err)
//	suite.Require().NotNil(data)
//	suite.Require().NotNil(count)
//}
//
//func (suite *MessageSuite) TestMessageContext() {
//	data, err := suite.MessageSvc.MessageContext(context.Background(), nil)
//	suite.Require().Nil(err)
//	suite.Require().NotNil(data)
//}

func (suite *MessageSuite) TestHighLightText() {
	var topics []Topic
	content := "本群目标是让大家都考个好成绩。这也是人生重要转折点，打好基础是必要的。群文件有老师总结的一些知识点，结合书上的习题多刷题课后答案解析也在文件中，同时还有网课视频陆续上传。上课没听懂也可以回看晚11点机器人再次核实，未邀请过的一律清理出群。群满后，关闭禁言，老师会在群里给大家义务解答同学们的各种问题。本群的所有资料免费，我们一起加油"
	topics = append(topics, Topic{
		IsDefault: false,
		Keywords:  []string{"目标", "成绩", "知识"},
		Name:      "两会",
	})
	topics = append(topics, Topic{
		IsDefault: true,
		Keywords:  []string{"人生", "网课", "清理"},
		Name:      "七一",
	})
	text, err := suite.MessageSvc.HighlightWords(topics, content)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(text)
	suite.Require().NotNil(text)
	suite.Require().Nil(err)
}

func (suite *MessageSuite) TestSearchFile() {
	var req = searchFileRequest{
		Types:          []string{"image"},
		StartTimestamp: 0,
		EndTimestamp:   0,
		Size:           20,
		Page:           2,
	}
	count, data, err := suite.MessageSvc.SearchFile(context.Background(), req)
	fmt.Println(err)
	fmt.Println(count)
	fmt.Println(data)
	//suite.Require().Nil(err)
	//suite.Require().NotNil(data)
	//suite.Require().NotNil(count)
}

func (suite *MessageSuite) TestSetTag() {
	var req = setTagRequest{
		Id:   "0018451AD433F66B8A098DF79A878E68000000000001D157",
		Tags: []string{"政治", "地理"},
	}
	data, err := suite.MessageSvc.SetTag(context.Background(), req)
	suite.Require().Nil(err)
	suite.Require().NotNil(data)
}
