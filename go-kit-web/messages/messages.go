package messages

import (
	"dash/collectors"
)

type Member struct {
	Id                   string                 `json:"id,omitempty"`
	NickName             string                 `json:"nickname,omitempty"`
	Age                  int64                  `json:"age,omitempty"`
	Sex                  string                 `json:"sex,omitempty"`
	Avatar               string                 `json:"avatar,omitempty"`
	CollectorId          string                 `json:"collectorId,omitempty"`
	CollectorOrgan       string                 `json:"collectorOrgan,omitempty"`
	Collectors           []collectors.Collector `json:"collectors,omitempty"`
	CaptureTimestamp     int64                  `json:"captureTimestamp,omitempty"`
	UsedNickname         []string               `json:"usedNickname,omitempty"`
	DataSource           string                 `json:"dataSource,omitempty"`
	MessageCount7        int64                  `json:"messageCount7,omitempty"`
	AlarmCount7          int64                  `json:"alarmCount7,omitempty"`
	Watched              bool                   `json:"watched,omitempty"`
	Tags                 []string               `json:"tags,omitempty"`
	Signature            string                 `json:"signature,omitempty"`
	Comment              string                 `json:"comment,omitempty"`
	UsedName             []string               `json:"usedName,omitempty"`
	Chatrooms            []Chatroom             `json:"chatrooms,omitempty"`
	Name                 string                 `json:"name,omitempty"`
	Email                string                 `json:"email,omitempty"`
	PhoneNumber          []string               `json:"phoneNumber,omitempty"`
	IdCard               string                 `json:"idCard,omitempty"`
	IdCardAvatar         string                 `json:"idCardAvatar,omitempty"`
	Address              []string               `json:"address,omitempty"`
	ResidenceAddress     string                 `json:"residenceAddress,omitempty"` //常住地
	CensusAddress        string                 `json:"censusAddress,omitempty"`    //户籍
	UsedSignature        []string               `json:"usedSignature,omitempty"`
	Aliases              []string               `json:"aliases,omitempty"`              //群内昵称
	LastMessageTimestamp int64                  `json:"lastMessageTimestamp,omitempty"` //最后发言时间
	JoinMethod           string                 `json:"joinMethod,omitempty"`
	Inviter              *Member                `json:"inviter,omitempty"`
	MsgCount             int64                  `json:"msgCount,omitempty"`
}

type Chatroom struct {
	Id               string    `json:"id,omitempty"`
	Name             string    `json:"name,omitempty"`
	Owner            Member    `json:"owner,omitempty"`
	Avatar           string    `json:"avatar,omitempty"`
	CollectorId      []string  `json:"collectorId,omitempty"`
	User             []string  `json:"user,omitempty"`
	CollectorOrgan   []string  `json:"collectorOrgan,omitempty"`
	CaptureTimestamp int64     `json:"captureTimestamp,omitempty"`
	DataSource       string    `json:"dataSource,omitempty"`
	MessageCount     int64     `json:"messageCount,omitempty"`
	Watched          bool      `json:"watched,omitempty"`
	Tags             []string  `json:"tags,omitempty"`
	Category         string    `json:"category,omitempty"`
	Comment          string    `json:"comment,omitempty"`
	UsedName         []string  `json:"usedName,omitempty"`
	Members          []*Member `json:"members,omitempty"`
}

type Message struct {
	Id               string   `json:"id,omitempty"`
	Type             string   `json:"type,omitempty"`
	Content          string   `json:"content,omitempty"`
	Highlight        string   `json:"highlight,omitempty"`
	FilePath         string   `json:"filePath,omitempty"`
	Timestamp        int64    `json:"timestamp,omitempty"`
	CollectorId      string   `json:"collectorId,omitempty"`
	CollectorOrgan   string   `json:"collectorOrgan,omitempty"`
	CaptureTimestamp int64    `json:"captureTimestamp,omitempty"`
	DataSource       string   `json:"dataSource,omitempty"`
	Watched          bool     `json:"watched,omitempty"`
	Topics           []Topic  `json:"topics,omitempty"`
	StringTopics     []string `json:"stringTopics,omitempty"`
	Member           Member   `json:"member,omitempty"`
	Chatroom         Chatroom `json:"chatroom,omitempty"`
	WarningScore     float64  `json:"warningScore,omitempty"`
	Score            float64  `json:"score,omitempty"`
	Index            int      `json:"index"`
}

type Topic struct {
	Id           string   `json:"id"`
	ParentId     string   `json:"parentId,omitempty"`
	IsDefault    bool     `json:"isDefault"`
	KeywordScore float64  `json:"keywordScore,omitempty"`
	Keywords     []string `json:"keywords,omitempty"`
	Name         string   `json:"name,omitempty"`
	Score        float64  `json:"score,omitempty"`
}

type File struct {
	Id                 string   `json:"id"`
	Name               string   `json:"name"`
	FirstSeenTimestamp int64    `json:"firstSeenTimestamp,omitempty"`
	LastSeenTimestamp  int64    `json:"lastSeenTimestamp,omitempty"`
	Size               int64    `json:"size,omitempty"`
	Type               string   `json:"type,omitempty"`
	MD5                string   `json:"md5,omitempty"`
	Format             string   `json:"format,omitempty"`
	Tags               []string `json:"tags,omitempty"`
	Count              int64    `json:"count,omitempty"`
	Path               string   `json:"path,omitempty"`
}
