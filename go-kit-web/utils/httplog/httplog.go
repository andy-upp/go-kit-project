package httplog

import (
	"github.com/go-kit/kit/log"
	"net/http"
	"time"
)

func HttpLog(logger log.Logger, next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		defer func() {
			_ = logger.Log(
				"addr", r.RemoteAddr,
				"path", r.URL.Path,
				"method", r.Method,
				"took", time.Now().Sub(start),
			)
		}()
		next.ServeHTTP(w, r)
	}
}
