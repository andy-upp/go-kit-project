package main

type config struct {
	MysqlDSN        string `yaml:"MysqlDSN"`
	RedisDSN        string `yaml:"RedisDSN"`
	RedisDB         int    `yaml:"RedisDB"`
	TopicVersionKey string `yaml:"TopicVersionKey"`
}
