USE dash;

INSERT INTO collector(id, name) VALUES('001', '001');
INSERT INTO collector(id, name) VALUES('002', '002');

INSERT INTO organization(id, name) VALUES('001', '深度好奇');
INSERT INTO organization(id, name) VALUES('002', 'deeply好奇');
INSERT INTO organization(id, name,parentId) VALUES('003', '研发部门','001');
INSERT INTO organization(id, name,parentId) VALUES('004', '研发小组','003');
INSERT INTO organization(id, name,parentId) VALUES('005', '设计部门','002');
INSERT INTO organization(id, name,parentId) VALUES('006', '设计小组','005');

INSERT INTO user(id, username, password, weChatId, avatar, name, organId) VALUES ('001', 'user001', '852438d026c018c4307b916406f98c62', 'unk', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '邓老师', '004');
INSERT INTO user(id, username, password, weChatId, avatar, name, organId) VALUES ('002', 'user002', '852438d026c018c4307b916406f98c62', 'unk', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '张老师', '006');
INSERT INTO user(id, username, password, weChatId, avatar, name, organId) VALUES ('003', 'user003', '852438d026c018c4307b916406f98c62', 'unk', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '宋老师', '004');
INSERT INTO user(id, username, password, weChatId, avatar, name, organId) VALUES ('004', 'user004', '852438d026c018c4307b916406f98c62', 'unk', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '董老师', '006');
INSERT INTO user(id, username, password, weChatId, avatar, name, organId) VALUES ('005', 'user005', '852438d026c018c4307b916406f98c62', 'unk', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '邓老师', '004');

INSERT INTO role(id, role) VALUES('001', 'admin');
INSERT INTO role(id, role) VALUES('002', 'admin');
INSERT INTO role(id, role) VALUES('003', 'user');
INSERT INTO role(id, role) VALUES('004', 'user');
INSERT INTO role(id, role) VALUES('005', 'user');

INSERT INTO collector_user(collectorId, userId) VALUES ('001', '001');
INSERT INTO collector_user(collectorId, userId) VALUES ('001', '002');
INSERT INTO collector_user(collectorId, userId) VALUES ('001', '003');
INSERT INTO collector_user(collectorId, userId) VALUES ('001', '004');
INSERT INTO collector_user(collectorId, userId) VALUES ('001', '005');

INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, version, keywords) VALUES('001', '民生', true, true, 1593490493382, 1593490493382, 1, '[{"op": "include", "keywords": "北京 天安门"}, {"op": "exclude", "keywords": "广告"}]');
INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, version, keywords) VALUES('002', '警务', true, true, 1593490493382, 1593490493382, 2, '[{"op": "include", "keywords": "北京 天安门"}, {"op": "exclude", "keywords": "广告"}]');
INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, parentId, version, keywords) VALUES('003', '社情民意', true, true, 1593490493382, 1593490493382, '001', 3, '[{"op": "include", "keywords": "北京 天安门"}, {"op": "exclude", "keywords": "广告"}]');
INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, parentId, version, keywords) VALUES('004', '民生', true, true, 1593490493382, 1593490493382, '001', 4, '[{"op": "include", "keywords": "北京 天安门"}, {"op": "exclude", "keywords": "广告"}]');
INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, parentId, version, keywords) VALUES('005', '肺炎', true, false, 1593490493382, 1593490493382, '001', 5, '[{"op": "include", "keywords": "北京 天安门"}, {"op": "exclude", "keywords": "广告"}]');
INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, parentId, version, keywords) VALUES('006', '就业', true, false, 1593490493382, 1593490493382, '001', 6, '[{"op": "include", "keywords": "北京 天安门"}]');
INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, parentId, version, keywords) VALUES('007', '养老', true, false, 1593490493382, 1593490493382, '002', 6, '[{"op": "include", "keywords": "北京 天安门"}]');
INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, parentId, version, keywords) VALUES('008', '保险', true, false, 1593490493382, 1593490493382, '002', 6, '[{"op": "include", "keywords": "北京 天安门"}]');
INSERT INTO topic(id, name, enabled, isDefault, createTimestamp, updateTimestamp, parentId, version, keywords) VALUES('009', '专题25', true, true, 1593490493382, 1593490493382, '002', 6, '[{"op": "include", "keywords": "北京 天安门"}]');

