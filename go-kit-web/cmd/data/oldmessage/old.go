package oldmessage

import (
	"dash/messages"
)

type Location struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

type Chatroom struct {
	Id                string   `json:"id,omitempty"`
	Name              string   `json:"name,omitempty"`
	OwnerId           string   `json:"ownerId,omitempty"`
	OwnerNickname     string   `json:"ownerNickname,omitempty"`
	DataSource        string   `json:"dataSource,omitempty"`
	ParentId          string   `json:"parentId,omitempty"`
	Comment           string   `json:"comment,omitempty"`
	Tags              []string `json:"tags,omitempty"`
	Activity          float64  `json:"activity,omitempty"`
	HotWords          []string `json:"hotWords,omitempty"`
	Avatar            string   `json:"avatar,omitempty"`
	Members           int64    `json:"members,omitempty"`
	Watched           bool     `json:"watched,omitempty"`
	Address           string   `json:"address,omitempty"`
	Location          Location `json:"location,omitempty"`
	Labels            []string `json:"labels,omitempty"`
	CreateAt          int64    `json:"createAt,omitempty"`
	AddressConfidence int64    `json:"addressConfidence,omitempty"`
	CollectorId       string   `json:"collectorId,omitempty"`
}

type Member struct {
	Id            string   `json:"id,omitempty"`
	Age           int64    `json:"age,omitempty"`
	Nickname      string   `json:"nickname,omitempty"`
	UsedNickname  []string `json:"usedNickname,omitempty"`
	Sex           string   `json:"sex,omitempty"`
	DataSource    string   `json:"dataSource,omitempty"`
	Address       string   `json:"address,omitempty"`
	Signature     string   `json:"signature,omitempty"`
	UsedSignature []string `json:"usedSignature,omitempty"`
	Tags          []string `json:"tags,omitempty"`
	Avatar        string   `json:"avatar,omitempty"`
	CollectorId   string   `json:"collectorId,omitempty"`
	Watched       bool     `json:"watched,omitempty"`
}

type MessageDetail struct {
	Text         string  `json:"text,omitempty"`
	RawText      string  `json:"rawText,omitempty"`
	Timestamp    int64   `json:"timestamp,omitempty"`
	CaptureTime  int64   `json:"captureTime,omitempty"`
	DataSource   string  `json:"dataSource,omitempty"`
	WarningScore float64 `json:"warningScore,omitempty"`
}

type Message struct {
	Id            string        `json:"id,omitempty"`
	Topics        []Topic       `json:"topics,omitempty"`
	TopicKeywords []string      `json:"topicKeywords,omitempty"`
	MessageDetail MessageDetail `json:"message,omitempty"`
	Member        Member        `json:"member,omitempty"`
	Chatroom      Chatroom      `json:"chatroom,omitempty"`
}

type Topic struct {
	Name  string  `json:"name"`
	Score float64 `json:"score"`
}

func Old2new(message Message) messages.Message {
	typeStr := "text"
	dataSource := "微信"
	topics := make([]messages.Topic, len(message.Topics))
	for i, topic := range message.Topics {
		topics[i] = messages.Topic{
			Id:           topic.Name,
			KeywordScore: topic.Score,
			Keywords:     message.TopicKeywords,
			Name:         topic.Name,
			Score:        topic.Score,
		}
	}
	newMessage := messages.Message{
		Id:               message.Id,
		Type:             typeStr,
		Content:          message.MessageDetail.Text,
		Timestamp:        message.MessageDetail.Timestamp,
		CaptureTimestamp: message.MessageDetail.CaptureTime,
		DataSource:       dataSource,
		Topics:           topics,
		Member: messages.Member{
			Id:       message.Member.Id,
			NickName: message.Member.Nickname,
			Sex:      message.Member.Sex,
		},
		Chatroom: messages.Chatroom{
			Id:   message.Chatroom.Id,
			Name: message.Chatroom.Name,
		},
	}
	return newMessage
}
