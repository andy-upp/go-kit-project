package main

import (
	"dash/users"
	"database/sql"
	"fmt"
)

var collectorId = "collector001"

func prepare(db *sql.DB) {
	_, err := db.Exec(
		`insert into collector(id, name) values(?, ?);`,
		collectorId, collectorId,
	)
	if err != nil {
		panic(err)
	}
	for i := 0; i < 10; i++ {
		userId := fmt.Sprintf("user%03d", i)
		password := users.MD5("nil")
		_, err = db.Exec(
			`INSERT INTO
            user(id,name,username,password,avatar)
            VALUES(?,?,?,?,?);`,
			userId,
			"邓老师",
			userId,
			password,
			"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
		)
		if err != nil {
			panic(err)
		}
		_, err = db.Exec(
			`INSERT INTO
            collector_user(collectorId, userId)
            VALUES(?,?);`,
			collectorId, userId,
		)
		if err != nil {
			panic(err)
		}
	}
}
