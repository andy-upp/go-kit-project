## chatroom 表

id
name
dataSource
avatar
ownerId

## [*] collector 表

id
name

## [*] category 表

id
name
parentId
createTimestamp

## [*] organization 表

id
name
parentId

## [*] role 表

id
role

## [*] user 表

id
username
password
weChatId
avatar
name
organId

## member 表

id
nickname
usedNickname
sex
avatar
dataSource
address
captureTimestamp
createTimestamp
updateTimestamp

## [*] topic 表

id
name
enabled
isDefault
createTimestamp
updateTimestamp
parentId
keywords
version

## 其他

chatroom_member

collector_member

collector_chatroom

[*] collector_user
