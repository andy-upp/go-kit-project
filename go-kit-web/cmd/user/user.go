package main

//一个小服务器测试文件
import (
	"dash/categories"
	"dash/collectormsg"
	"dash/chatrooms"
	"dash/jwt"
	"dash/members"
	"dash/users"
	"database/sql"
	"github.com/go-redis/redis/v8"
	"github.com/olivere/elastic/v7"
	"log"
	"net/http"
)

func main() {
	//var r http.Handler
	//{
	//r = mux.NewRouter()
	//r = jwt.NewJWTMiddleware(nil, logger,jwts)
	//}

	//sqlClient
	db, err := sql.Open("mysql",
		"root:123456@tcp(127.0.0.1:3306)/dash")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		_ = db.Close()
	}()
	//redisClient
	rc := redis.NewClient(&redis.Options{
		Network: "tcp",
		Addr:    "192.168.31.207:6379",
		DB:      0,
	})
	//---esClient
	ec, err := elastic.NewSimpleClient(elastic.SetURL("http://192.168.31.36:19200"))
	if err != nil {
		log.Fatal(err)
	}
	//serverInit
	logger := categories.NewLogger()
	jwts := jwt.NewJWTService(jwt.SIGNKEY)
	ur := users.NewUserRepository(db, logger)
	us := users.NewUserService(logger, ur, jwts)
	mr := members.NewMemberRepository(db, logger, rc, ec)
	ms := members.NewMemberService(logger, mr)
	cr := categories.NewCategoryRepository(db, logger)
	cs := categories.NewCategoryService(cr)
	dosr := collectormsg.NewCollectorRepository(db, logger)
	ts := collectormsg.NewTopParamsService(cs, logger)
	dos := collectormsg.NewOrganizationService(dosr, logger)

	chatroomrepo := chatrooms.NewChatroomRepository(db, rc, ec, logger)
	chatroomsvc := chatrooms.NewChatroomService(chatroomrepo, logger)
	mux := http.NewServeMux()
	mux.Handle("/api/v1/user/profile", users.MakeUserHandler(us, logger))
	mux.Handle("/api/v1/user/", users.MakeLoginHandler(us))
	mux.Handle("/api/v1/member/", members.MakeHandler(ms))
	mux.Handle("/api/v1/category", categories.MakeHandler(cs))
	mux.Handle("/api/v1/query/", collectormsg.MakeQueryHandler(dos, ts))
	mux.Handle("/api/v1/chatroom/", chatrooms.MakeHandler(chatroomsvc))
	_ = logger.Log("transport", "http", "address", "8889")
	log.Fatal(http.ListenAndServe(":8889", mux))
}
