# 把kafka的数据导入到数据库

## 更新规则

text_message, other_message

可以解析出messageReport对象，存储到es的message索引

---

collector_join_chatroom

collectorJoinChatroomReport对象包含chatroom，owner，把owner更新到mysql，chatroom更新到mysql，把群和群主更新到mysql的chatroom_member

---

collector_leave_chatroom

更新chatroom_member的quit字段为true

---

member_join_chatroom

memberJoinChatroomReport对象包含chatroom，owner，member，把owner更新到mysql，member更新到mysql，chatroom更新到mysql，把群和群主/群和成员更新到mysql的chatroom_member，quit字段设为false

---

member_leave_chatroom

memberLeaveChatroomReport对象包含chatroom，owner，member，把owner更新到mysql，member更新到mysql，chatroom更新到mysql，把群和群主/群和成员更新到mysql的chatroom_member，群/群主quit字段设为false，群/群成员quite字段设为true

---

chatroomMemberReport对象包含了chatroom数组，数组里解析出来的chatroom，owner，member更新到mysql
